" UI
: colorscheme desert
set ruler
set number
set background=dark
set t_Co=256
set cursorline
set backspace=indent,eol,start
set fdm=indent

" Golang
" gofmt *.go files after saving
autocmd BufWritePre *.go :Fmt
filetype off
filetype plugin indent off
set rtp+=$GOROOT/misc/vim
filetype plugin indent on
filetype plugin on
syntax on
filetype indent on

" Bundle
set nocompatible
let mapleader=","
set rtp+=/usr/local/share/vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'
Bundle 'Valloric/YouCompleteMe'
Bundle 'cespare/vim-golang'
Bundle 'dgryski/vim-godef'
let g:godef_split=2

" no backup
set nobackup
set noswapfile
set nowb

" encoding
set encoding=utf8

" auto, smart indent
set ai
set si

" highlight search
set hlsearch

" open file in the place last modified
autocmd BufReadPost *
			\ if line("'\"")>0&&line("'\"")<=line("$") |
			\exe "normal g'\"" |
			\ endif

" go tags
Bundle 'majutsushi/tagbar'
nmap <F8> :TagbarToggle<CR>
let g:tagbar_type_go = {
    \ 'ctagstype' : 'go',
    \ 'kinds'     : [
        \ 'p:package',
        \ 'i:imports:1',
        \ 'c:constants',
        \ 'v:variables',
        \ 't:types',
        \ 'n:interfaces',
        \ 'w:fields',
        \ 'e:embedded',
        \ 'm:methods',
        \ 'r:constructor',
        \ 'f:functions'
    \ ],
    \ 'sro' : '.',
    \ 'kind2scope' : {
        \ 't' : 'ctype',
        \ 'n' : 'ntype'
    \ },
    \ 'scope2kind' : {
        \ 'ctype' : 't',
        \ 'ntype' : 'n'
    \ },
    \ 'ctagsbin'  : 'gotags',
    \ 'ctagsargs' : '-sort -silent'
    \ }

